-- Carson Jobe

-- The database and all tables are structured incorrectly

-- User IDs. Ideally the Userpass would be concealed in some way
CREATE TABLE IF NOT EXISTS Users (
    User_ID int NOT NULL AUTO_INCREMENT,
    Username varchar(20) NOT NULL,
    Userpass varchar(20) NOT NULL,
    Email varchar(20) NOT NULL,
    PRIMARY KEY (User_ID)
);

/*
List of all of the Categories
Planned to include:
Animals, Nature, Art, Architecture, Movies, Food, and Technology
Ideally, would list all wallpapers with a given category
*/
CREATE TABLE IF NOT EXISTS Categories (
    Category_ID int NOT NULL AUTO_INCREMENT,
    Category_Name varchar(20) NOT NULL,
    PRIMARY KEY (Category_ID)
    -- UNIQUE KEY (Category_Name)
);

/*
List of all SubCategories
Planned to include:
Animals - Mammals, birds, insects, other
Nature - Mountains, forest, plains, water
Art - Painting, Minimal, Abstract, line, Fantasy
 
CREATE TABLE IF NOT EXISTS SubCategories (
    Sub_Cat_ID int NOT NULL AUTO_INCREMENT,
    Sub_Cat_Name varchar(20) NOT NULL,
    Parent_Cat int NOT NULL,
    PRIMARY KEY (Sub_Cat_ID),
    FOREIGN KEY (Parent_Cat) REFERENCES Categories(Category_ID) ON UPDATE CASCADE ON DELETE CASCADE
);*/

/*
Contains a list of all resolutions of wallpapers
3864x2160
1920x1080
2800x1800
1680x1050
1024x768
Vertical
*/
CREATE TABLE IF NOT EXISTS Resolutions (
    Resolution_ID int NOT NULL AUTO_INCREMENT,
    Resolution_Name varchar(10) NOT NULL,
    PRIMARY KEY (Resolution_ID)
    -- UNIQUE KEY (Resolution_Name)
);


/*
A list of dominant colors of an image. Would be determined server side by another script.
Would contain primary and secondary colors as well as black and white
*/
CREATE TABLE IF NOT EXISTS DominantColors (
    Color_ID int NOT NULL AUTO_INCREMENT,
    Color_Name varchar(10) NOT NULL,
    PRIMARY KEY (Color_ID)
    -- UNIQUE KEY (Color_Name)
);

/*
The bulk of operations would be performed on this table.
Filename would be determined by the primary key "Wallpaper"
Most attributes are foreign keys, which doesn't work.
Number of downloads would be determined by a download button. This is a bad idea because
a site user could just right click which would register...or just 'save link as'. 
"Rating" would be determined by users who rated the image. From this attribute along with
the "user" attribute, a user could have an average rating.
*/
CREATE TABLE IF NOT EXISTS EachWallpaper (
    Wallpaper_ID int NOT NULL AUTO_INCREMENT,
    Wall_Name varchar(50),
    Category int,
    -- Sub_Category int,
    Resolution int, 
    Dominant_Color int,
    Upload_User int,
    Number_Downloads int,
    Rating int,
    PRIMARY KEY (Wallpaper_ID),
    FOREIGN KEY (Category) REFERENCES Categories(Category_ID) ON UPDATE CASCADE ON DELETE CASCADE,
    -- FOREIGN KEY (Sub_Category) REFERENCES SubCategories(Sub_Cat_ID) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (Resolution) REFERENCES Resolutions(Resolution_ID) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (Dominant_Color) REFERENCES DominantColors(Color_ID) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (Upload_User) REFERENCES Users(User_ID) ON UPDATE CASCADE ON DELETE CASCADE
);